//main heart of system, restfull with power of couchdb
//
const lgsServer = "http://195.130.109.85/test/";
const lgsFormat = "JSON";

var nodes_list;
var num_layer = 0;
var layers;
var params = "";
                
function addXMLRequestCallback(callback){
    var oldSend, i;
    if( XMLHttpRequest.callbacks ) {
        // we've already overridden send() so just add the callback
        XMLHttpRequest.callbacks.push( callback );
    } else {
        // create a callback queue
        XMLHttpRequest.callbacks = [callback];
        // store the native send()
        oldSend = XMLHttpRequest.prototype.send;
        // override the native send()
        XMLHttpRequest.prototype.send = function(){
            // process the callback queue
            // the xhr instance is passed into each callback but seems pretty useless
            // you can't tell what its destination is or call abort() without an error
            // so only really good for logging that a request has happened
            // I could be wrong, I hope so...
            // EDIT: I suppose you could override the onreadystatechange handler though
            for( i = 0; i < XMLHttpRequest.callbacks.length; i++ ) {
                XMLHttpRequest.callbacks[i]( this );
            }
            // call the native send()
            oldSend.apply(this, arguments);
        }
    }
}

// e.g.
addXMLRequestCallback( function( getLGSRemoteResource ) {
    console.log( getLGSRemoteResource.responseText ); // (an empty string)
});
addXMLRequestCallback( function( getLGSRemoteResource ) {
    console.dir( getLGSRemoteResource ); // have a look if there is anything useful here
});

//connection with server for testing
function getLGSRemoteResource(method,resource, lgs_callback, num){
	var url = lgsServer + resource;
	var get_data3 = 'l='+url;
  
  function run_appl_3(data3){
  alert("final url : "+url);
  }
  
	var GET_ajax_file = "http://195.130.109.85";
        $.ajax({
                 type: "GET",
                  //   url: '"../'+GET_ajax_file+'"',
                                     url: GET_ajax_file,
                                     async:true,
                                    // dataType: "xml",
                                     data: get_data3,
                                     success:  run_appl_3
                                 });// .get	
	request=new XMLHttpRequest();
	request.onreadystatechange=function(){
		if (request.readyState==4){
			//toggleVisibility('progress_bar');
			if (request.status==200){
				text=request.responseText;
				alert('text : '+text);
				alert('  num(einai to id tou kathe layer) : '+num);
				console.log("text : "+text);
				lgs_callback(text, num);
			} else {
				alert('error');
			}
		}
	}
	request.open(method, url, true);
	request.send("");
}

//take layers lists
function parseLayerList(raw_data, num){
	alert('2');
	var layer_type;
	var name;
	var description;
	var since;
	var id = 0;
	var altitude;
	var distance;
	var position_since;
	
	layer_type = name = description = since = position_since = "";
	distance = 0.0;
	altitude = 0.0;
	
	var data = eval('(' + raw_data + ')');
	if(data["code"] != 200)
		return;
	
	alert('raw_data from layar '+JSON.stringify(raw_data));
	console.log('raw_data from layar  :  '+JSON.stringify(raw_data));
	
	var raw_layers = data["results"];
	layers = new Array();
	var layers_length = 0;
	var i = 0;
	for (i = 0; i < raw_layers.length; i++){
		var raw_layer = raw_layers[i];
		if(!raw_layer["id"])
			continue; 
		id = raw_layer["id"];
		alert('id from layer'+id);
		name = raw_layer["name"];
		alert('name from layer'+name);
		var layer = new GenericLayer(id, layer_type, name, description,  since, "", "", altitude, distance, position_since );
		layers[layers_length] = layer;
		layers_length = layers.length;
		alert('to layer witch created : '+JSON.stringify(layer));
	}
	
	if(layers.length > 0)
		getNodesList("", "0", 10.0, 0, 10);  //selection of user from distance,number of pois that shown	
}



//take nodes from server
function parseNodesList(raw_data, layer_id){
    //alert('raw_data from server root :  '+JSON.stringify(raw_data));
	var nodes = new Array();
	var data = eval('(' + raw_data + ')');
		
    //alert('raw_data apo node : '+raw_data);
	console.log('raw_data  :  '+raw_data);
	                                                                   
	if(data["code"] != 200)
		return;
	var raw_nodes = data["results"];
	//	alert('raw_nodes from layar :  '+JSON.stringify(raw_nodes));
	console.log('raw_nodes :  '+JSON.stringify(raw_nodes));
		
	var i = 0;
	var nodes_length = 0;
	for(i = 0; i<raw_nodes.length; i++){
		var node = parseNode(raw_nodes[i], layer_id);
	
		if(node){
			nodes_length = nodes.length;
			nodes[nodes_length] = node;
		//alert('nodes_length :  '+JSON.stringify(nodes_length));	
		//alert('nodes[nodes_length] :  '+JSON.stringify(nodes[nodes_length]));	
		}
	}
	
	if(nodes.length > 0){
		nodes_list = nodes_list.concat(nodes);
	//	alert('nodes_list_concat '+nodes_list.length);
	}
	num_layer++;	
	//alert('nodes_list :  '+JSON.stringify(nodes_list));	
	console.log('nodes_list :  '+JSON.stringify(nodes_list));	
	//alert('nodes :  '+JSON.stringify(nodes));
	console.log('nodes :  '+JSON.stringify(nodes));
	//alert('node :  '+JSON.stringify(node));
	console.log('node :  '+JSON.stringify(node));

	gettingNodeSystem();
}


//start to get layar list from couchdb server
function getLayersList(){
	if(!nodes_list)
	nodes_list = new Array();
	nodes_list.length = 0;
    //alert('come from ar_location');
	
	//connection of couchdbtest
	var lat="";
	var i;
	var poi="";
	layer_id=804;
    var i = 0;
	var nodes_length = 0;

		//connect to server
		$.couch.urlPrefix = "http://******:*****@195.130.109.109:10000";
		
		//show function from couchdb	
		$.couch.db("gar_teiath").view("teiath_ar/pois", {
			
			//get results of pois to visualization
		    success: function(data) {
		 	 console.log(data);
		 		for( i in data.rows )
                    { 
					//num of poi
				     poi=data.rows[i].id;
					// poi=str_replace('"','',poi);
  		            // }
		          
		          						//open document of poi
										$.couch.db("gar_teiath").openDoc(poi,{
										 success: function(data) {
										   //get  
										    function parseNode(raw_node, layer_id){

	var node;
	var type = "";
	var since;
	var position_since;
	var username;
	var latitude;
	var longitude;
	var altitude;
	var radius;
	var distance;
	var id = 0;
	
	latitude = longitude = radius = distance = -1.0;
	altitude = 0.0;
	since = position_since = "";

	//poi must have for minimum type,id,since,position
	if(!raw_node["type"])
		return false;
	type = raw_node["type"];
	id = raw_node["id"];
	since = raw_node["since"];
	
	if(raw_node["position"]){
		latitude = raw_node["position"]["latitude"];
		longitude = raw_node["position"]["longitude"];
		altitude = raw_node["position"]["altitude"];
		position_since = raw_node["position"]["since"];
		radius = raw_node["position"]["radius"];
	}
	
	//type of poi person
	if (type.toLowerCase() == "person"){
		var name = raw_node["first_name"];
		var lastName = raw_node["last_name"];
		var username = raw_node["username"];
		var status = "";
		var status_since = "";
		if(raw_node["status"]){
			status = raw_node["status"]["message"];
			status_since = raw_node["status"]["since"];
		}
		var email = raw_node["email"];
		var birthday = raw_node["birthday"];
		var avatarId = "";
		var avatarUrl = "";
		if(raw_node["avatar"]){
			avatarId = raw_node["avatar"]["photo_id"];
			avatarUrl = raw_node["avatar"]["photo_url"];
		}
		var country = "";
		var postCode = "";
		if(raw_node["position"]){
			country = raw_node["position"]["country"];
			postCode = raw_node["position"]["post_code"];
		}
		node = new User(id, name, lastName, username, email, status, latitude, longitude, altitude, radius, since, country, postCode, birthday, friends, avatarId, avatarUrl, status_since, position_since);
	}
	
	//type of poi photo
	else if (type.toLowerCase() == "photo"){
		var name = raw_node["name"];
		var description = raw_node["description"];
		var external_info;
		var url = "";

		if(!id){
			external_info = new ExternalInfo();
			if(raw_node["external_info"]){
				external_info.info_url = raw_node["external_info"]["info_url"];
				external_info.media_url = raw_node["external_info"]["photo_url"];
				external_info.photo_thumb_url = raw_node["external_info"]["photo_thumb"];
				url = raw_node["external_info"]["photo_thumb"];
			}
			
							//test for take right url			
							var get_data4 = 'l='+external_info;
							  function run_appl_4(data4){
							}
							var GET_ajax_file = "http://195.130.109.85";
								$.ajax({
									 type: "GET",
									  //   url: '"../'+GET_ajax_file+'"',
														 url: GET_ajax_file,
														 async:true,
														// dataType: "xml",
														 data: get_data4,
														 success:  run_appl_4
													 });// .get
							}else{
							

							external_info = new ExternalInfo();
								if(raw_node["external_info"]){
									external_info.info_url = raw_node["external_info"]["info_url"];
									external_info.media_url = raw_node["external_info"]["photo_url"];
									external_info.photo_thumb_url = raw_node["external_info"]["photo_thumb"];
									url = raw_node["external_info"]["photo_thumb"];
								}
							//take url
							//	url = raw_node["url"];
							//	url = lgsServer + "layer/" + layer_id + "/node/" + id + "/image/";			
							}
		
		node = new Photo(id, latitude, longitude, altitude, radius, name, description, url, since, username, position_since, external_info);
	}
	//type of poi note	
	else if (type.toLowerCase() == "note"){
		var title = raw_node["title"];
		var body = raw_node["text"];
		node = new Note(id, title, body, latitude, longitude, altitude, radius, since, position_since);
	}
	//type of poi sound	
	else if (type.toLowerCase() == "sound"){
		var name = raw_node["name"];
		var description = raw_node["description"];
		var path = "";
		var url = lgsServer + "layer/" + layer_id + "/node/" + id + "/sound_file/";
		node = new Audio(id, name, description, url, path, latitude, longitude, altitude, radius, since, position_since);
	}
     //type of poi video	
	else if (type.toLowerCase() == "video"){
		var name = raw_node["name"];
		var description = raw_node["description"];
		var external_info = new ExternalInfo();
		
		if(raw_node["external_info"]){
			external_info.info_url = raw_node["external_info"]["info_url"];
			external_info.media_url = raw_node["external_info"]["video_url"];
			external_info.photo_thumb_url = raw_node["external_info"]["video_thumb_url"];
		}
		
		node = new Video(id, name, description, "", latitude, longitude, altitude, radius, since, position_since, external_info);
	}
	//anything else poi type/general poi
	else{
		node = new GeoNode(id, latitude, longitude, altitude, radius, since, position_since);
	}	
	return node;
}
						    
				//take poi result 	    
				 alert("results : "+JSON.stringify(data));
					var nodes = new Array();
					var raw_nodes = data;
				//take all pois results
					var node = parseNode(raw_nodes,layer_id);
						//take sum of pois			
						if(node){
									nodes_length = nodes.length;
									nodes[nodes_length] = node;
								}
						//conacat all poi in a list for view													
							if(nodes.length > 0){
								nodes_list = nodes_list.concat(nodes);
							}
							num_layer++;
																			
						alert('nodes_list :  '+JSON.stringify(nodes_list));	
						console.log('nodes_list :  '+JSON.stringify(nodes_list));	
						
						//alert('nodes :  '+JSON.stringify(nodes));
						console.log('nodes :  '+JSON.stringify(nodes));
						//alert('node :  '+JSON.stringify(node));
						console.log('node :  '+JSON.stringify(node));
						//send pois to div of html index for showing
							gettingNodeSystem();					     
						},
						error: function(status) {
							alert(status);
						}
					});	          		        
}		   },
		    error: function(status) {
		        console.log(status);
		    },
		    reduce: false
		});	
			
//getLGSRemoteResource("GET", "layer/list/?format=JSON", parseLayerList);  
//getLGSRemoteResource("GET", "json.php?format=JSON", parseLayerList);
	
}

function getNodesList(pattern, category, distance, page, elems){
	var i = 0;
	//for static test of development
	params = "/search.php?search=" + pattern + 
								   "&category=" + category +
								   "&latitude=" + 37.9895698 + 
								   "&longitude=" + 23.7649895 + 
								   "&page=" + page +
								   "&elems=" + elems +
								   "&radius=" + distance + "&format=JSON";
	
	
	 
	   	var get_data1 = 'l='+params;
		  function run_appl_1(data1){
		  alert('params :   '+params);
		  }
    var GET_ajax_file = "http://195.130.109.85";
        $.ajax({
                 type: "GET",
                  //   url: '"../'+GET_ajax_file+'"',
                                     url: GET_ajax_file,
                                     async:true,
                                    // dataType: "xml",
                                     data: get_data1,
                                     success:  run_appl_1
                                 });// .get

	num_layer = 0;
	gettingNodeSystem();
}

function gettingNodeSystem(){
	showElement('loading_div');
}